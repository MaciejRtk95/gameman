# Gameman changelog

_Every merge to master is a new version_

You want to report a bug or suggest a feature? Open an issue at https://www.gitlab.com/matsaki95/gameman

### Pre-Alpha 0 "Initialising initializers"

Initial commit:

- Added readme placeholder
- Added main.cpp placeholder
- Added license
